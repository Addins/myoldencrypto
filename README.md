# README #

This is the repository of an image encryption app  using classic cryptography. It is an android app. It uses olden-crypto, a lib I built. 
This code is kind of poc (proof of concept) app. I coded it as part of my learning path on building android app and classic cryptography.
I learnt about storing files in external storage and publicly accessible, using SharedPreferences to store last state (specially storing last key used), etc.
