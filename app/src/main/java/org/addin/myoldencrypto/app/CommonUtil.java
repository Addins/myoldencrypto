package org.addin.myoldencrypto.app;

import android.os.Environment;
import android.util.Log;
import org.addin.crypto.classic.core.SimpleKey;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.util.Arrays;

/**
 * Created by addin on 6/23/15.
 */
public class CommonUtil {
    public static String convertSimpleKeyToString(SimpleKey key){
        JSONArray arr = new JSONArray(Arrays.asList(key.getKey()));
        return arr.toString();
    }

    public static SimpleKey<int[][]> convertStringToSimpleKey(String strKey){
        if(!"".equals(strKey)){
            try {
                int[][] mtx = new int[16][16];
                JSONArray jsonArray = new JSONArray(strKey);
                jsonArray = jsonArray.getJSONArray(0);
                for(int i = 0;i<16;i++){
                    JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                    for(int j= 0; j< 16;j++){
                        mtx[i][j] = jsonArray1.getInt(j);
                    }
                }
                return new SimpleKey<int[][]>(mtx);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static File getAlbumStorageDir(String albumName){
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES),albumName
        );
        if(!file.mkdirs()){
            Log.e(MainActivity.TAG, "Directory not created.");
        }
        return file;
    }
}
