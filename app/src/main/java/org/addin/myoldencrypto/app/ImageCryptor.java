package org.addin.myoldencrypto.app;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import ar.com.hjg.pngj.PngjInputException;
import org.addin.crypto.classic.core.SimpleKey;
import org.addin.crypto.classic.image.PngImageSuperEncryption;

import java.io.File;

/**
 * Created by addin on 6/3/15.
 */
public class ImageCryptor extends AsyncTask<String,Void, String> {

    private String input;
    private int mode;

    private PngImageSuperEncryption cipher;
    private SimpleKey<int[][]> key;

    private ImageCryptorHandler cryptorHandler;

    private double processTime;

    @Override
    protected String doInBackground(String... paths) {
        input = paths[0];
        String output = "";
        try {
            output = processing(input, mode);
        }catch (PngjInputException e){
            e.printStackTrace();
        }
        return output;
    }

    private String processing(String path, int mode) {
        String[] split = path.split("\\/");
        String outputPath = CommonUtil.getAlbumStorageDir("myoldencrypto").getAbsolutePath()+File.separator+"mode"+mode+"_"+split[split.length-1];
        cipher = new PngImageSuperEncryption(path,outputPath);
        cipher.setKey(key);

        // start counting time
        long startTime = System.nanoTime();

        cipher.process(mode);

        // stop counting time
        long stopTime = System.nanoTime();
        long elapsedTime = stopTime-startTime;
        processTime = (double)elapsedTime / 1000000000.0;
        Log.d("Measure cipher process in mode "+mode,processTime+" sec.");

        return outputPath;
    }

    @Override
    protected void onPostExecute(String path) {
        cryptorHandler.onProcessFinished(path, processTime);
    }

    public void setCryptorHandler(ImageCryptorHandler cryptorHandler) {
        this.cryptorHandler = cryptorHandler;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public void setKey(SimpleKey<int[][]> key) {
        this.key = key;
    }
}
