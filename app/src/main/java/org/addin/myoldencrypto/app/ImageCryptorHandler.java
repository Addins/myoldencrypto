package org.addin.myoldencrypto.app;

/**
 * Created by addin on 6/3/15.
 */
public interface ImageCryptorHandler {
    public void onProcessFinished(String path, double processTime);
}
