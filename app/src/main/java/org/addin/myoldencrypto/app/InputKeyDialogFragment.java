package org.addin.myoldencrypto.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;

/**
 * Created by addin on 9/4/15.
 */
@SuppressLint("NewApi")
public class InputKeyDialogFragment extends DialogFragment {

    /* The activity that creates an instance of this dialog fragment must
    *  implement this interface in order to receive event callbacks.
    *  Each method passes the DialogFragment in case the host needs to query it. */
    public interface InputKeyDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    InputKeyDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the InputKeyDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            mListener = (InputKeyDialogListener) activity;
        } catch (ClassCastException ex){
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()+" must implement InputKeyDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.dialog_input_key, null))
                // Add action buttons
                .setPositiveButton(R.string.change_key, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Send the positive button event back to the host activity
                        mListener.onDialogPositiveClick(InputKeyDialogFragment.this);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Send the negative button event back to the host activity
                        mListener.onDialogNegativeClick(InputKeyDialogFragment.this);
                    }
                });

        builder.setTitle(R.string.title_dialog_inputkey);
        return builder.create();
    }
}
