package org.addin.myoldencrypto.app;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import org.addin.crypto.classic.core.SimpleKey;
import org.addin.crypto.classic.core.util.SpiralSquareTextKeyGen;
import org.addin.crypto.classic.image.PngImageSuperEncryption;


public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, InputKeyDialogFragment.InputKeyDialogListener {

    public static final String TAG = "MyOldenCrypto";

    private static final int TAKE_PICTURE = 1234;
    private static final int LATEST_PICTURE = 1235;
    private static final int SELECT_PICTURE = 1236;

    private static final String ENCRYPTING_STRING = "encrypting...";
    private static final String DECRYPTING_STRING = "decrypting...";

    private static final String PREFKEY_PLAIN_IMG_PATH = "PLAIN_IMG_PATH";
    private static final String PREFKEY_CIPHERED_IMG_PATH = "CIPHERED_IMG_PATH";
    private static final String PREFKEY_CIPHER_KEY = "CIPHER_KEY";

    private static Bitmap bitmap;
    private static String bitmapPath;
    private Uri uri;

    private SimpleKey<int[][]> key;

    private ImageView imageView1;
    private ImageButton cameraButton;
    private ImageButton encryptButton;
    private ImageButton decryptButton;
    private ImageButton browseButton;
    private TextView textView1;
    private ProgressBar progressBar1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView1 = (ImageView) findViewById(R.id.imgVw1);
        cameraButton = (ImageButton) findViewById(R.id.btnCamera);
        encryptButton = (ImageButton) findViewById(R.id.btnEncrypt);
        decryptButton = (ImageButton) findViewById(R.id.btnDecrypt);
        browseButton = (ImageButton) findViewById(R.id.btnBrowse);
        textView1 = (TextView) findViewById(R.id.textView1);
        progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);

        cameraButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, TAKE_PICTURE);
            }
        });

        encryptButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                encrypt();
            }
        });

        decryptButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                decrypt();
            }
        });

        browseButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getSupportLoaderManager().destroyLoader(SELECT_PICTURE);
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/png");
                startActivityForResult(intent,SELECT_PICTURE);
            }
        });

    }

    private void decrypt() {
        if(null == key){
            Toast.makeText(this,"No key set!",Toast.LENGTH_SHORT).show();
            return;
        }
        if(null!=bitmapPath && !"".equals(bitmapPath)){
            if(!isExternalStorageWritable()){
                Toast.makeText(this,"External storage is not writable.",Toast.LENGTH_SHORT).show();
            }

            ImageCryptor cryptor = new ImageCryptor();
            cryptor.setKey(key);
            cryptor.setMode(PngImageSuperEncryption.DECRYPT_MODE);
            cryptor.setCryptorHandler(new ImageCryptorHandler() {
                @Override
                public void onProcessFinished(final String path, double processTime) {
                    progressBar1.setVisibility(View.INVISIBLE);
                    textView1.setText("");

                    Toast.makeText(MainActivity.this, "finished in "+processTime+" sec.", Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Execution Time")
                            .setMessage("finished in "+processTime+" sec.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                                    intent.putExtra(ResultActivity.INTENT_RESULTPATH_EXTRA, path);
                                    startActivity(intent);
                                }
                            })
                            .create().show();
                }
            });
            cryptor.execute(bitmapPath);
            progressBar1.setVisibility(View.VISIBLE);
            textView1.setText(DECRYPTING_STRING);
        }
    }

    private void encrypt() {

        if(null==key){
//            generateKey();
            showInputKeyDialog();
            return;
        }

        if(null!=bitmapPath && !"".equals(bitmapPath)){
            if(!isExternalStorageWritable()){
                Toast.makeText(this,"External storage is not writable.",Toast.LENGTH_SHORT).show();
            }

            storeSomething(PREFKEY_PLAIN_IMG_PATH,bitmapPath);

            ImageCryptor cryptor = new ImageCryptor();
            cryptor.setKey(key);
            cryptor.setMode(PngImageSuperEncryption.ENCRYPT_MODE);
            cryptor.setCryptorHandler(new ImageCryptorHandler() {
                @Override
                public void onProcessFinished(final String path, double processTime) {
                    progressBar1.setVisibility(View.INVISIBLE);
                    textView1.setText("");

                    storeSomething(PREFKEY_CIPHERED_IMG_PATH, path);

                    Toast.makeText(MainActivity.this, "finished in "+processTime+" sec.", Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Execution Time")
                            .setMessage("finished in "+processTime+" sec.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                                    intent.putExtra(ResultActivity.INTENT_RESULTPATH_EXTRA, path);
                                    startActivity(intent);
                                }
                            })
                            .create().show();
                }
            });
            cryptor.execute(bitmapPath);

//                    storeSomething(PREFKEY_CIPHERED_IMG_PATH,path);
            progressBar1.setVisibility(View.VISIBLE);
            textView1.setText(ENCRYPTING_STRING);
        }
    }

    /*private void generateKey(){
        key = new SimpleKey<int[][]>(Arrays.copyOf(new SimpleKeyGen(16).generateMatrix(),16));
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_genkey) {
            //generateKey();
            showInputKeyDialog();
            //Toast.makeText(this,"new key generated.",Toast.LENGTH_SHORT).show();
            return true;
        }

        if(id == R.id.action_last_plain_image){
            String path = retrieveSomething(PREFKEY_PLAIN_IMG_PATH);
            if(!"".equals(path)) {
                bitmapPath = path;
                bitmap = BitmapFactory.decodeFile(bitmapPath);
                imageView1.setImageBitmap(bitmap);
            }
            return true;
        }

        if(id == R.id.action_last_encrypted_image){
            String path = retrieveSomething(PREFKEY_CIPHERED_IMG_PATH);
            if(!"".equals(path)) {
                bitmapPath = path;
                bitmap = BitmapFactory.decodeFile(bitmapPath);
                imageView1.setImageBitmap(bitmap);
            }
            return true;
        }

        if(id == R.id.action_input_key){
            showInputKeyDialog();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(null != key)
            storeCipherKey(key);
    }

    @Override
    protected void onStart() {
        super.onStart();
        key = retrieveCipherKey();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == TAKE_PICTURE) {

            } else if (requestCode == SELECT_PICTURE) {
                Uri selectedUri = data.getData();

                /*Bundle bundle = new Bundle();
                bundle.putString("data", selectedUri.toString());
                getSupportLoaderManager().initLoader(SELECT_PICTURE, bundle,
                        this);*/

                bitmapPath = getRealPathFromUri(this, selectedUri);
                bitmap = BitmapFactory.decodeFile(bitmapPath);
                imageView1.setImageBitmap(bitmap);
            }
        }
    }

    public boolean isExternalStorageWritable(){
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state))
            return true;
        return false;
    }

    private void storeSomething(String key, String value){
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,value);
        editor.commit();
    }

    private String retrieveSomething(String key){
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        return preferences.getString(key,"");
    }

    private void storeCipherKey(SimpleKey<int[][]> key){
        String s = CommonUtil.convertSimpleKeyToString(key);
        storeSomething(PREFKEY_CIPHER_KEY, s);
    }

    private SimpleKey<int[][]> retrieveCipherKey(){
        String strKey = retrieveSomething(PREFKEY_CIPHER_KEY);
        return  CommonUtil.convertStringToSimpleKey(strKey);
    }



    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        switch (i) {
            case LATEST_PICTURE:
                return new CursorLoader(this,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[] {
                        MediaStore.Images.Media._ID,
                        MediaStore.Images.Media.DATA }, null, null,
                        MediaStore.Images.Media._ID + " DESC");
            case SELECT_PICTURE:
                return new CursorLoader(this, Uri.parse(bundle.getString("data")),
                        new String[] { MediaStore.Images.Media._ID,
                                MediaStore.Images.Media.DATA }, null, null, null);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor.moveToFirst()) {
            int id = cursor.getInt(cursor
                    .getColumnIndex(MediaStore.Images.Media._ID));
            bitmapPath = cursor.getString(cursor
                    .getColumnIndex(MediaStore.Images.Media.DATA));
            bitmap = BitmapFactory.decodeFile(bitmapPath);
            imageView1.setImageBitmap(bitmap);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        bitmap = null;
        bitmapPath = null;
        imageView1.setImageBitmap(null);
    }

    @SuppressLint("NewApi")
    private void showInputKeyDialog(){
        DialogFragment dialog = new InputKeyDialogFragment();
        dialog.show(getFragmentManager(),"InputKeyDialogFragment");
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        @SuppressLint({"NewApi", "LocalSuppress"})
        EditText keywordEdTxt = (EditText) dialog.getDialog().findViewById(R.id.keyword);
        key = new SimpleKey<int[][]>(new SpiralSquareTextKeyGen(16,keywordEdTxt.getText().toString()).generateMatrix());
        Toast.makeText(this,"new key generated using text "+keywordEdTxt.getText(),Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("NewApi")
    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        dialog.getDialog().cancel();
    }

    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
