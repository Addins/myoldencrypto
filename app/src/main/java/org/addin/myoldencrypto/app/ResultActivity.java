package org.addin.myoldencrypto.app;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;


public class ResultActivity extends AppCompatActivity {

    public static final String INTENT_RESULTPATH_EXTRA = "resultPath";
    private static final int LOAD_IMAGE = 1941;

    private ImageView imgVwResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        imgVwResult = (ImageView) findViewById(R.id.imgVwResult);

        Intent intent = getIntent();
        if(null != intent){
            String path = intent.getStringExtra(INTENT_RESULTPATH_EXTRA);
            imgVwResult.setImageBitmap(BitmapFactory.decodeFile(path));
        }
    }

}
